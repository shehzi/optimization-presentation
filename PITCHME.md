## Optimizing nutrient intake using Linear Programming Techniques  

Shehzaman Khatib  
Consultant, SJRI 

---

### Breaking down nutrients

* All foods can be represented universally as nutrients - energy, protein, iron etc

* All nutrient requirements can be represented by the same variables

---

### Add some data structures

* Lets make vector of nutrients
$$\begin{bmatrix}E \\\ P \\\ C \\\ I \\\ B12 \\\ \vdots \end{bmatrix}$$

---

### Representing food $F_i$

$$ F_i = \begin{bmatrix}E_i \\\ P_i \\\ C_i \\\ I_i \\\ B12_i \\\ \vdots \end{bmatrix}$$

---

### Lets just add foods together

$$\sum_{i=1}^{250} \begin{bmatrix}E_i \\\ P_i \\\ C_i \\\ I_i \\\ B12_i \\\ \vdots \end{bmatrix}$$

---

### But add an integer weight $z_i$

$$\sum_{i=1}^{250} z_i \times \begin{bmatrix}E_i \\\ P_i \\\ C_i \\\ I_i \\\ B12_i \\\ \vdots \end{bmatrix}$$

---

### More concisely we represent nutrients from $n$ foods and $m$ nutrients

<!-- $$ \sum_{i=1}^{n} z_i \times F_{ij}, j=1,\cdots,m $$ -->

$$ \sum_{i=1}^{n}z_i \times {F_i}_j, j=1,\cdots,m $$
---

### Recommended Diet $R$ 

* Calculated using age, weight, height, physical activity level etc.

$$ R = \begin{bmatrix}E \\\ P \\\ C \\\ I \\\ B12 \\\ \vdots \end{bmatrix}$$

(same representation!)

---

### Recommended Energy (Emperical formula) $E_i$

$$ E_i = 662 - 9.53 \times A + P (15.91 \times W + 539.6 \times H)  $$

* E is energy in kCal/day
* A is age in years
* P is physical activity number based on BMI (body mass index) and lifestyle
* W is weight in Kg
* H is height in m

---

### Linear Programming

* In general, called linear optimization too
* Mathematics used for solving problems with linear relationships (only $x$, no $x^2$)
* One objective function, and a bunch of constraints

---

### Mixed Integer Linear Programming

* Linear Programming where linear relationships have integers and non-integers
* Generally, they will have integer weights but variables can be non-integers
* $3x_i, 4x_i, 121234x_i$ but no $1.2x_i, 4.87236x_i$

---

### Optimization Libraries (bunch of code to solve these complicated things)
* COIN-OR - CBC - free for everyone
* SCIP - free for research 
* GUROBI - paid license
* ... many more

---

### Working Backwards - Capacitated Factory Location

The Capacitated Facility Location problem determines where, amongst $m$ locations, to place facilities and assigns the production of $n$ products to these facilities
in a way that (in this variant) minimizes the wasted capacity of facilities. Each
product $i = 1,\cdots, n$ has a production requirement $r_i$ and each facility has the
same capacity $C$

---

$$ min \sum_{j=1}^{m} w_j $$

such that,

$$ \sum_{j=1}^{m} {x_j}_i = 1, i = 1,\cdots, n $$ (each product produced)

$$ \sum_{i=1}^{n}r_i{x_j}_i + w_j = C, j = 1, \cdots, m $$ (capacity at location j)

$w_j$ is wasted capacity at location $j$

---

### Lets turn it around

$$ min \sum_{j=1}^{m} w_j $$

such that,

$$ \sum_{j=1}^{m} {F_j}_i = 1, i = 1,\cdots, n $$ 

$$ \sum_{i=1}^{n} z_i \times {F_i}_j + w_j = R_j, j=1,\cdots, m $$

$w_j$ is wasted nutrient $j$

---

### Difference

* Every nutrient has a different requirement
* Fixed factory capacity $C$ is now variable nutrient capacity $R_j$

---

### What is the recommendation?

* $z_i$ is the integer value for each food $F_i$

* Two stage solution to remove over used nutrients:
    * Solve once for eliminating food that contribute to nutrient beyond RDA
    * Solve once more for satisfying the remaining nutrients

---

### Challenges

* Solution feasibility
* Increasing number of foods
* Increasing number of nutrients
* Learn food preferences
* Create database of foods and their nutrients

---

### Thank you!